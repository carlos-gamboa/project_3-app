import AuthActions from './AuthActions';

export default function AuthReducer(state = [], action) {
  switch (action.type) {
  case AuthActions.LOGIN:
    return state.concat([action.text]);
  default:
    return state;
  }
}