import React, { Component } from 'react';
import './App.scss';
import { Switch, Route } from 'react-router-dom';
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Login from './pages/login/Login';
import NotFound from './pages/not-found/NotFound';
//import PrivateRoute from './components/private-route/PrivateRoute';

class App extends Component {
  render() {
    const header = <Header></Header>;
    const footer = <Footer />;

    return (
      <React.Fragment>
        { header }
        <Switch>
          <Route exact path='/login' component={Login} />
          <Route component={NotFound} />
        </Switch>
        { footer }
      </React.Fragment>
    );
  }
}

export default App;
