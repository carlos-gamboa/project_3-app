import React, { Component } from 'react';
import './Header.scss';
import logo from '../../assets/images/branding/logo-black.png';
import { withRouter, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

class Header extends Component {

  constructor() {
    super();
    this.state = {
      showMenu: false
    };
  }

  handleToggleMobileMenu() {
    this.setState({
      showMenu: !this.state.showMenu
    });
  }

  handleLogout(event) {
    event.preventDefault();
    //this.props.onLogout();
    this.props.history.replace('/login');
  }

  render() {
    const { showMenu } = this.state;

    const navClasses = ['header__nav'];
    let mobileNavIcon = 'close';

    if (!showMenu) {
      navClasses.push('header__nav--hidden');
      mobileNavIcon = 'menu';
    }

    return (
      <header className='header'>
        <div className='header__branding'>
          <img src={logo} alt='Logo' className='header__logo'/>
        </div>
        <div className="header__mobile">
          <button className='header__mobile-toggle' onClick={(event) => {
            this.handleToggleMobileMenu(event);
          }}><i className='material-icons header__icon'>{mobileNavIcon}</i></button>
        </div>
        <nav className={navClasses.join(' ')}>
          <ul className='header__list'>
            <li className='header__list-item'><NavLink to='/' exact activeClassName='header__link--active' className='header__link'>Home</NavLink></li>
            <li className='header__list-item'><NavLink to='/login' exact activeClassName='header__link--active' className='header__link'>Boat</NavLink></li>
            <li className='header__list-item'><NavLink to='/airplane' exact activeClassName='header__link--active' className='header__link'>Airplane</NavLink></li>
            <li className='header__list-item'><button onClick={(event) => this.handleLogout(event)} className='header__link'>Logout</button></li>
          </ul>
        </nav>
      </header>
    );
  }
}

Header.propTypes = {
  history: PropTypes.object,
  onLogout: PropTypes.func
};

export default withRouter(Header);
