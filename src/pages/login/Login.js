import React, { Component } from 'react';
import './Login.scss';
import FormValidator from '../../helpers/FormValidator';

class Login extends Component {
  constructor() {
    super();

    this.validator = new FormValidator([
      { 
        field: 'email', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Email is required.' 
      },
      { 
        field: 'email',
        method: 'isEmail', 
        validWhen: true, 
        message: 'That is not a valid email.'
      },
      { 
        field: 'password', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Password is required.'
      }
    ]);

    this.state = {
      email: '',
      password: '',
      validation: this.validator.isValid(),
    };

    this.submitted = false;
  }

  handleInputChange = (event) => {
    event.preventDefault();

    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = (event) => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      // handle actual form submission here
    }
  }

  getFormInputClasses = (validation, fieldToValidate) =>{
    let classes = ['login__input'];
    if (validation[fieldToValidate].isInvalid) {
      classes.push('login__input--invalid');
    }
    return classes.join(' ');
  }

  getValidationError = (validation, fieldToValidate) => {
    const validationError = (validation[fieldToValidate].isInvalid) ?
      <span className="login__validation-error">{validation[fieldToValidate].message}</span>
      : null;
    
    return validationError;
  }

  render() {
    // If the form has been submitted at least once.
    let validation = this.submitted ?                         
      this.validator.validate(this.state) : // Check validity every time we render.
      this.state.validation; // Otherwise just use what's in state

    return (
      <section className='login'>
        <div className='login__container'>

          <h1 className='login__heading'>Login</h1>

          <form className="login__form">

            <div className='login__form-control'>
              <label htmlFor="email" className='login__label'>Email address</label>
              <input type="email" className={this.getFormInputClasses(validation, 'email')}
                name="email"
                placeholder="example@konradgroup.com"
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'email')}
            </div>


            <div className='login__form-control'>
              <label htmlFor="password" className='login__label'>Password</label>
              <input type="password" className={this.getFormInputClasses(validation, 'email')}
                name="password"
                onChange={this.handleInputChange}
              />
              {this.getValidationError(validation, 'password')}
            </div>

            <button onClick={this.handleFormSubmit} className="login__button">
          Sign up
            </button>
          </form>
        </div>
      </section>
    );
  }
}
export default Login;